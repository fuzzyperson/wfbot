
import redis
from redisearch import Client, TextField, AutoCompleter, TagField, Suggestion
from ircframebot import network

from typing import Set

REDIS_HOST = '127.0.0.1'
REDIS_PORT = 2500
_POOL = redis.ConnectionPool(host=REDIS_HOST, port=REDIS_PORT)

ALLOWED_SET = {str, int, bytes}

FORCE_OPER = '15cc33a7_cloaked'


def get_connection() -> redis.Redis:
    """
    コネクションプールからコネクターを得る
    :return:
    """

    return redis.Redis(connection_pool=_POOL)


def db_search(_type: str, name: str, allow_fuzzy: bool = False) -> list:
    searcher = Client(_type, host=REDIS_HOST, port=REDIS_PORT, conn=get_connection())
    search_text = name

    try:
        result = searcher.search(search_text).docs
        return result

    except redis.exceptions.ResponseError as e:
        return []


def db_set_set(_type: str, key: str, value: Set[str]) -> Set[str]:
    """
    セット _type:key にstrが入ったSetを値としてセットする

    結果として以下の形になる
    _type:key = Set {
        Value1,
        Value2,
        Value3...
    }
    :param _type: タイプ　カテゴリとも
    :param key: キー　検索用のキー
    :param value: 値　セット
    :return: 新しく追加されたセット一覧
    """

    combined_key = '{}:{}'.format(_type, key.replace(' ', '_'))
    conn = get_connection()
    for item in value:
        conn.sadd(combined_key, item)

    return value


def db_add_set(_type: str, key: str, value: str) -> Set[bytes]:
    """
    特定のセットに対しvalueを値として追加する
    :param _type: タイプ　カテゴリとも
    :param key: キー　検索用のキー
    :param value: 追加したい値
    :return: 追加された後のセット
    """
    combined_key = '{}:{}'.format(_type, key.replace(' ', '_'))
    get_connection().sadd(combined_key, value)

    return db_get_set(_type, key)


def db_del_set(_type: str, key: str, value: str) -> Set[bytes]:
    """
    特定のセットからvalueを削除する
    :param _type: タイプ　カテゴリとも
    :param key: キー　検索用のキー
    :param value: 削除したい値
    :return: 削除された後のセット
    """
    combined_key = '{}:{}'.format(_type, key.replace(' ', '_'))
    get_connection().srem(combined_key, value)

    return db_get_set(_type, key)

def db_ismember_set(_type: str, key: str, value: str) -> bool:
    """
    指定されたセットの中に要素があるかチェックする
    :param _type: タイプ　カテゴリとも
    :param key: キー　検索用のキー
    :param value: 存在を確かめたい値
    :return: 存在するか否か
    """
    combined_key = '{}:{}'.format(_type, key.replace(' ', '_'))
    return get_connection().sismember(combined_key, value)


def db_get_set(_type: str, key: str) -> Set[bytes]:
    """
    指定されたキーに存在する要素をセットとして返す
    :param _type: タイプ　カテゴリとも
    :param key: キー
    :return: セット
    """
    combined_key = '{}:{}'.format(_type, key.replace(' ', '_'))
    conn = get_connection()

    if not conn.exists(combined_key):
        return set()

    item = conn.smembers(combined_key)
    return item


def db_set_hash(_type: str, key: str, value: dict) -> str:
    """
    ハッシュ _type に key の形で値をセットする

    結果として以下のような形になる
    _type:key = HASH {
        _hash_key: value,
        _hash_key: value,
        ...
    }

    :param _type: タイプ　カテゴリとも
    :param key: キー　検索用のキー
    :param value: 値　大体辞書
    :return: 新しく追加されたキー
    """
    combined_key = '{}:{}'.format(_type, key.replace(' ', '_'))
    conn = get_connection()
    for _hash_key, _value in value.items():
        conn.hset(
            combined_key,
            _hash_key,
            _value if type(_value) in ALLOWED_SET else str(_value)
        )

    return combined_key


def db_set_searchable(_type: str, target: str, search_key: str) -> bool:
    document_id = '{}:{}'.format(_type, target.replace(' ', '_'))
    if not get_connection().exists(document_id):
        raise KeyError('Document Does not Exist.')

    client = Client(_type, host=REDIS_HOST, port=REDIS_PORT, conn=get_connection())

    try:
        client.create_index([TextField('SearchIndex')])

    except:
        pass

    return client.add_document(document_id, SearchIndex=search_key, nosave=True, replace=True)


def db_get_hash(_type: str, key: str) -> dict:
    """
    Redisからキーをゲットする
    :param _type:
    :param key:
    :return:
    """
    result = get_connection().hgetall('{}:{}'.format(_type, key.replace(' ', '_')))

    return {
        key.decode() if isinstance(key, bytes) else key:
        val.decode() if isinstance(val, bytes) else val
        for key, val in result.items()
    }


def db_get_missiontypes(mt_str: str) -> str:
    """
    ミッションタイプの翻訳を返す
    :param mt_str: MT_から始まる文字列
    :return: 文字列
    """

    return db_get_hash('missionTypes', mt_str)['value']


def db_get_solnode(node: str) -> dict:
    """
    SolNodeの翻訳結果を返す
    :param node: SolNodeX のような文字列
    :return: 辞書
    """

    return db_get_hash('solNodes', node)


def db_translate_item(unique_name: str) -> str:
    """
    /Lotus/Weapon/... みたいな言語を翻訳する
    :param unique_name:
    :return:
    """
    try:
        item = db_get_hash('Languages', unique_name.lower())
        return item['value']

    except KeyError:
        return unique_name


def db_update_market_data() -> None:
    """
    Rivenとプラチナの相場のアップデート
    :return: なし
    """

    market_data = network.get_market_weapon_data()

    for item in market_data:
        url_name = item['url_name']
        item_name = item['item_name']

        db_set_hash('Price', url_name, item)
        db_set_searchable('Price', url_name, item_name)

    riven_data = network.get_riven_market_data()

    for key, value in riven_data.items():
        db_set_hash('Riven', key, value)
        db_set_searchable('Riven', key, key)


def db_update_drop_data() -> None:
    """
    Relicとドロップ系のデータをアップデートする
    :return:
    """

    bp_data, mod_data = network.get_drop_tables()
    relic_data = network.get_relic_table()

    for item in bp_data:
        item_data = set()
        for enemy in item['enemies']:
            message = '{} - {}%'.format(enemy['enemyName'], enemy['total_chance'])
            item_data.add(message)

        db_set_set('Drop', item['itemName'], item_data)
        db_set_searchable('Drop', item['itemName'], item['itemName'])

    for mod in mod_data:
        item_data = set()

        for enemy in mod['enemies']:
            message = '{} - {}%'.format(enemy['enemyName'], enemy['total_chance'])
            item_data.add(message)

        db_set_set('Drop', mod['modName'], item_data)
        db_set_searchable('Drop', mod['modName'], mod['modName'])

    for prime_name, relic_drop in relic_data.items():
        relic_set = set()
        for relic_name, relic_chance in relic_drop.items():
            message = '{} - {}%'.format(relic_name, relic_chance)
            relic_set.add(message)

        db_set_set('Relic', prime_name, relic_set)
        db_set_searchable('Relic', prime_name, prime_name)


def db_data_update() -> None:
    """
    データベースのデータをアップデートする
    :return:
    """

    # ホストのバックアップを取る
    # backup = db_get_set('Oper', 'Oper')
    # backup = {i.decode() for i in backup}
    conn = get_connection()
    conn.flushall()


    official_data = (
        ('solNodes', network.get_solnodes()),  # 太陽系データ
        ('missionTypes', network.get_mission_types()),  # ミッションタイプ
        ('Languages', network.get_item_data()),  # アイテムの言語
    )

    for _type, data in official_data:
        print('Updating', _type)
        for key, value in data.items():
            db_set_hash(_type, key, value)

    print('Updating Market')
    db_update_market_data()
    print('Updating Drop')
    db_update_drop_data()

    # オペレータセットし直し
    # db_set_set('Oper', 'Oper', backup)
    if not db_ismember_set('Oper', 'Oper', FORCE_OPER):
        db_add_set('Oper', 'Oper', FORCE_OPER)

# coding: UTF-8
# Imports

from irc.client import NickMask
from irc.bot import SingleServerIRCBot
from ircframebot.commands import Command
from ircframebot import database
SERVER = "seyaroka.dip.jp"
CHANNEL = "#VIPWarframe"
NICKNAME = "bottest"
PORT = 6667

class MarketBot(SingleServerIRCBot):
    def __init__(self):
        SingleServerIRCBot.__init__(self,
                                    server_list=[(SERVER, PORT)],
                                    nickname=NICKNAME,
                                    realname=NICKNAME,
                                )
        self.ready = False

    def say(self, msg: str, to: str) -> None:
        self.connection.privmsg(to, "\x0314" + msg + "\x03")

    def update(self):
        """
        データをアップデートする
        :return: なし
        """
        self.say('アップデート開始', CHANNEL)
        self.ready = False
        database.db_data_update()
        self.say('アップデート完了', CHANNEL)
        self.ready = True

    # ============ #
    # イベント処理 #
    # ============ #

    def on_welcome(self, c, e):
        c.join(CHANNEL)

    def on_join(self, connection, e):
        print(str(e))
        nick = NickMask(e.source)
        joinerhost = nick.host
        current_channel = self.channels[e.target]

        if nick != connection.get_nickname() and current_channel.is_oper(connection.get_nickname()):
            if database.db_ismember_set('Oper', 'Oper', joinerhost):
                connection.mode(e.target, '+o {name}'.format(name=nick.nick))

        if nick.nick == connection.get_nickname():
            self.say("!help か !u か !usage で使い方を調べてね", CHANNEL)
            self.ready = True

    def on_privmsg(self, connection, event):
        if not self.ready:
            return 0

        if NickMask(event.source).nick == NICKNAME:
            return

        comm = Command(event)
        done = comm.operate()

        print(done, comm.result)
        if done:
            for result in comm.result:
                self.say(result.message, result.to)

    on_pubmsg = on_privmsg

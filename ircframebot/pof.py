
import time

_COMMANDS = {}

def activated_at(react_at: list):
    def _func(func):
        for comm in react_at:
            _COMMANDS[comm] = func.__name__

        def _wrapper(*args, **kwargs):
            return func(*args, **kwargs)

        return _wrapper
    return _func


class Pof:

    @activated_at(['!pof'])
    def pof(self):
        print('Pof Activated')

    def run(self, command: str):
        func_name = _COMMANDS[command]

        if hasattr(self, func_name) and callable(getattr(self, func_name)):
            function = getattr(self, func_name)
            function()


pof = Pof()

pof.run('!pof')
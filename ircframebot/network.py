
import json
import requests
from bs4 import BeautifulSoup
from string import Template
MARKET_URL = "https://warframe.market/"
WORLD_STATE_URL = "http://content.warframe.com/dynamic/worldState.php"
MOD_DROP_URL = "https://drops.warframestat.us/data/modLocations.json"
BP_DROP_URL = "https://drops.warframestat.us/data/blueprintLocations.json"
RELIC_DROP_URL = "https://drops.warframestat.us/data/relics.json"
PARSE_DATA_URL = "https://api.warframestat.us/languages"
SOL_NODES_URL = 'https://api.warframestat.us/solNodes'
MISSION_TYPES_URL = 'https://api.warframestat.us/missiontypes'

MARKET_API_URL = Template("https://api.warframe.market/v1/items/$item_name/orders")
RIVEN_API_URL = 'https://n9e5v4d8.ssl.hwcdn.net/repos/weeklyRivensPC.json'


def get_json_data(url: str) -> dict:
    """
    JSONを返すAPIにリクエストを飛ばす
    :param url: URL
    :return: 辞書
    """
    item = requests.get(url, allow_redirects=False, timeout=5)
    if item.status_code != 200:
        return {}

    return item.json()


def get_drop_tables() -> (list, list):
    """
    ドロップテーブルを返す
    :return: BPのリスト、MODのリスト
    """
    bp_table = get_json_data(BP_DROP_URL)['blueprintLocations']
    mod_table = get_json_data(MOD_DROP_URL)['modLocations']

    for item in bp_table:
        name = item['itemName']

        for drop in item['enemies']:
            drop['total_chance'] = (float(drop['enemyItemDropChance']) / 100 * float(drop['chance']) / 100) * 100

    for item in mod_table:
        name = item['modName']

        for drop in item['enemies']:
            drop['total_chance'] = (float(drop['enemyModDropChance']) / 100 * float(drop['chance']) / 100 ) * 100

    return bp_table, mod_table


def get_relic_table() -> dict:
    """
    レリックのテーブルを返す
    例: { Akstiletto Prime Barrel: [Axi A1, Axi...] }
    :return: 辞書
    """
    relic_table = get_json_data(RELIC_DROP_URL)['relics']

    result = {}
    for relic in filter(lambda x: x['state'] == 'Intact', relic_table):
        relic_name = '{tier} {relicName}'.format(tier=relic['tier'], relicName=relic['relicName'])

        for reward in relic['rewards']:
            item_name = reward['itemName']

            if item_name not in result:
                result[item_name] = {}

            result[item_name][relic_name] = reward['chance']

    return result

def get_solnodes() -> dict:
    """
    SolNodesを取得する
    :return: 辞書
    """

    return get_json_data(SOL_NODES_URL)


def get_fissures() -> list:
    """
    亀裂を取得する
    :return: リスト
    """
    return get_json_data(WORLD_STATE_URL)["ActiveMissions"]


def get_mission_types() -> dict:
    """
    ミッション用の名前を取得する
    :return:
    """

    return get_json_data(MISSION_TYPES_URL)


def get_item_data() -> dict:
    """
    Warframeで使われるユニークなアイテム表記とゲーム内命名の変換用データを取得する
    :return: 辞書
    """
    return get_json_data(PARSE_DATA_URL)


def get_baro_ki_teer() -> dict:
    """
    Baro Ki'Teerの情報を取得する
    :return: 辞書
    """
    return get_json_data(WORLD_STATE_URL)['VoidTraders'][0]


def get_item_cheapest_sell(item_name: str) -> int:
    """
    特定のアイテムの最低の売り金額をintで取得する
    売り注文が存在しなかった場合は-1を返す

    :param item_name: アイテムの名前
    :return: 売り注文の最低金額　売りがない場合は-1
    """
    full_url = MARKET_API_URL.substitute(item_name=item_name)
    result = get_json_data(full_url)
    result = result['payload']['orders']
    if not result:
        return -1

    result_plat = [i["platinum"] for i in result
                   if i["order_type"] == "sell"
                   and i["user"]["status"] == "ingame"]

    if not result_plat:
        return -1

    return min(result_plat)


def get_market_weapon_data() -> dict:
    raw_data = requests.get(MARKET_URL, allow_redirects=False, timeout=5)
    soup = BeautifulSoup(raw_data.text, "lxml").find("script", id="application-state").string
    jsoned_data = json.loads(soup)

    return jsoned_data['items']['en']


def get_riven_market_data() -> dict:
    rivens = get_json_data(RIVEN_API_URL)

    parsed_rivens = {}
    for item in rivens:
        item['rerolled'] = int(item['rerolled'])
        key = item['compatibility']

        if item['compatibility'] is None:
            key = item['itemType']

        elif item['compatibility'].startswith('<ARCHWING>'):
            item['compatibility'] = item['compatibility'].rstrip('<ARCHWING> ')
            key = item['compatibility']

        if item['rerolled']:
            key += ' (Rerolled)'

        parsed_rivens[key] = item

    return parsed_rivens

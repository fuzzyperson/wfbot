

import pendulum
import binascii

from collections import namedtuple
from irc.client import Event, NickMask
from ircframebot import network, database

Result = namedtuple('Result', ['to', 'message'])
__COMMANDS__ = {}

# TODO: __COMMANDS__ これがどうも気にかかる　すごい無理矢理感漂う
# それとやっぱりhasattr + getattr は乱用したくない　時間があったら別の方法を探せ

ERAS = {
    'lith': {'lith', 't1', '1', 'l'},
    'meso': {'meso', 't2', '2', 'm'},
    'neo': {'neo', 't3', '3', 'n'},
    'axi': {'axi', 't4', '4', 'a'},
}


def parse_time(time: pendulum.Duration) -> str:
    """
    PendulumのDurationオブジェクトを日本人向けにパースする
    :param time: Durationオブジェクト
    :return: パースされた文字列
    """
    parsed = []
    if time.remaining_days > 0:
        parsed.append('{}日'.format(time.remaining_days))

    if time.hours > 0:
        parsed.append('{}時間'.format(time.hours))

    if time.minutes > 0:
        parsed.append('{}分'.format(time.minutes))

    if time.remaining_seconds > 0:
        parsed.append('{}秒'.format(time.remaining_seconds))

    return ' '.join(parsed)


def activated_at(react_at: list):
    """
    コマンド用デコレータ 関数名を辞書に登録し、Commandsクラスから参照できるようにする
    :param react_at: Commandsの中のメソッドが反応するコマンド一覧
    :return: デコレートされた関数
    """
    def _func(func):
        for comm in react_at:
            __COMMANDS__[comm] = func.__name__

        def _wrapper(*args, **kwargs):
            return func(*args, **kwargs)

        return _wrapper
    return _func


class Command:
    """
    コマンドクラス
    BOTがメッセージを受け取った際、このインスタンスが生成される
    """
    FUZZY_OPTION = ' --fuzzy'

    def __init__(self, event: Event):
        self.is_privmsg = event.type == 'privmsg'
        self.message_in = event.target

        nickmask = NickMask(event.source)
        self.sender = nickmask.nick
        self.sender_host = nickmask.host

        self.message = event.arguments[0]

        self.respond_to = (
            self.sender if self.is_privmsg
            else self.message_in
        )

        self.result = []
        self.commands = {}

    def _parse_command(self) -> (str, str):
        """
        メッセージをコマンドの形にパースする
        :return: パースされたコマンド、及び引数
        """
        command, _, arguments = self.message.partition(' ')
        return command, arguments

    def _is_command(self, func_name: str) -> bool:
        """
        Commandsクラスで反応可能なコマンドかチェック
        :param func_name: コマンドの名前
        :return: 反応可能か否か
        """
        return hasattr(self, func_name) and callable(getattr(self, func_name))

    def fuzzy_strip(self, arguments: str) -> (bool, str):
        """
        argumentsが FUZZY_OPT で指定されたオプションが付いていた場合それを引き剥がす
        :param arguments: ユーザーの入力
        :return: Fuzzyオプション付きかどうか、ついてた場合は引き剥がされたArguments
        """

        allow_fuzzy = False

        if arguments.endswith(self.FUZZY_OPTION):
            allow_fuzzy = True
            arguments = arguments.strip(self.FUZZY_OPTION)

        return allow_fuzzy, arguments

    def operate(self):
        """
        処理を実行する
        #TODO: これ必要？？
        :return: なし
        """
        command, arguments = self._parse_command()
        func_name = __COMMANDS__.get(command, None)
        if func_name is None:
            return False

        if not self._is_command(func_name):
            return False

        function = getattr(self, func_name)
        return function(arguments)

    @activated_at(['!p', '!price'])
    def price(self, arguments: str) -> bool:
        """
        !p or !price [アイテム名] : アイテムのプラチナ相場を検索
        :param arguments: アイテムの名前
        :return: 完了したかどうか
        """
        fuzzy_option = ' --fuzzy'

        self.result.clear()
        if not arguments:
            self.result.append(Result(self.respond_to, '失敗: 引数がないよ　（使い方 - !p [アイテム名]）'))
            return True

        if len(arguments) <= 2:
            self.result.append(Result(self.respond_to, '失敗: 引数が短すぎる'))
            return True

        allow_fuzzy, arguments = self.fuzzy_strip(arguments)

        search_results = database.db_search('Price', arguments)

        if not search_results:
            self.result.append(Result(self.respond_to, '失敗: アイテムが存在しない'))
            return True

        for item in search_results[:4]:
            network_response = network.get_item_cheapest_sell(item.url_name)
            if network_response == -1:
                result_msg = '{itemname} : (売り値なし: 誰も売ってない)'.format(itemname=item.item_name)

            else:
                result_msg = '{itemname} : {price} pt'.format(itemname=item.item_name, price=network_response)

            self.result.append(Result(self.respond_to, result_msg))

        return True

    @activated_at(['!d', '!drop'])
    def drop(self, arguments: str) -> bool:
        """
        !d or !drop [アイテム名] : アイテムのドロップ率の検索
        :param arguments: アイテム名
        :return: 完了したかどうか
        """
        if not arguments:
            self.result.append(Result(self.respond_to, '失敗: 引数がないよ　（使い方 - !d [アイテム名]）'))
            return True

        drop_data = database.db_search('Drop', arguments)

        if not drop_data:
            self.result.append(Result(self.respond_to, '失敗: データがない'))
            return True

        if len(drop_data) > 2:
            if not self.is_privmsg:
                self.result.append(Result(self.message_in, '成功: 長文になるから個人チャット見ろ'))
                self.respond_to = self.sender

        for drop in drop_data[:3]:
            name = drop.id[5:].replace('_', ' ')
            self.result.append(Result(self.respond_to, '{name} を落とす敵一覧'.format(name=name)))

            drop_set = database.db_get_set('Drop', name)

            if not drop_set:
                self.result.append(Result(self.respond_to, 'データなし'))
                continue

            for drop_item in drop_set:
                self.result.append(Result(self.respond_to, drop_item.decode()))

        return True

    @activated_at(['!riven'])
    def riven(self, arguments: str) -> bool:
        """
        !riven [武器名] : 武器のRivenの相場を検索
        :param arguments: 武器名
        :return: 完了したかどうか
        """
        self.result.clear()
        if not arguments:
            self.result.append(Result(self.respond_to, '失敗: 引数がないよ　（使い方 - !riven [武器名]）'))
            return True

        if len(arguments) <= 1:
            self.result.append(Result(self.respond_to, '失敗: 引数が短すぎる　（最低２文字）'))
            return True

        riven_data = database.db_search('Riven', arguments)

        if not riven_data:
            self.result.append(Result(self.respond_to, '失敗: データがない'))
            return True

        print(riven_data[0])
        full_name = riven_data[0].compatibility if riven_data[0].compatibility != 'None' else riven_data[0].itemType
        self.result.append(Result(self.respond_to, '{name}のRivenの相場:'.format(name=full_name)))

        for riven in riven_data[:2]:
            result = '{is_rerolled}: Avg {avg} pt (max {max} pt / min {min} pt / med {med} pt)'.format(
                is_rerolled='リロール後' if bool(int(riven.rerolled)) else 'リロール前',
                avg=riven.avg,
                max=riven.max,
                min=riven.min,
                med=riven.median
            )
            self.result.append(Result(self.respond_to, result))

        return True

    @activated_at(['!relic'])
    def relic(self, arguments: str) -> bool:
        """
        !relic [アイテム名] : 指定アイテムを落とすレリック名を検索
        :param arguments: アイテム名
        :return: 完了したかどうか
        """
        if not arguments:
            self.result.append(Result(self.respond_to, '失敗: 引数がないよ　（使い方 - !relic [素材名]）'))
            return True

        relic_data = database.db_search('Relic', arguments)

        if not relic_data:
            self.result.append(Result(self.respond_to, '失敗: データがない'))
            return True

        if len(relic_data) > 2:
            if not self.is_privmsg:
                self.result.append(Result(self.message_in, '成功: 長文になるから個人チャット見ろ'))
                self.respond_to = self.sender

        for item in relic_data[:4]:
            name = item.id[6:].replace('_', ' ')
            self.result.append(Result(self.respond_to, '{name} を落とすレリック一覧'.format(name=name)))

            relic_set = database.db_get_set('Relic', name)

            if not relic_set:
                self.result.append(Result(self.respond_to, 'データなし'))
                continue

            for relic in relic_set:
                self.result.append(Result(self.respond_to, relic.decode()))

        return True

    @activated_at(['!f', '!fissure'])
    def fissure(self, era: str) -> bool:
        """
        !f or !fissure [ランク名 or 数字] or !fissure [ランク名 or 数字] : 現在の亀裂チェック
        :param _: 亀裂の世代名
        :return: 完了したかどうか
        """

        missions = network.get_fissures()
        era = era.lower()

        if era in ERAS['lith']:
            fissures = [i for i in missions if i["Modifier"] == "VoidT1"]
            self.result.append(Result(self.respond_to, "LITHの亀裂一覧:"))

        elif era in ERAS['meso']:
            fissures = [i for i in missions if i["Modifier"] == "VoidT2"]
            self.result.append(Result(self.respond_to, "MESOの亀裂一覧:"))

        elif era in ERAS['neo']:
            fissures = [i for i in missions if i["Modifier"] == "VoidT3"]
            self.result.append(Result(self.respond_to, "NEOの亀裂一覧:"))

        elif era in ERAS['axi']:
            fissures = [i for i in missions if i["Modifier"] == "VoidT4"]
            self.result.append(Result(self.respond_to, "AXIの亀裂一覧:"))

        else:
            self.result.append(Result(self.respond_to, "指定の亀裂が存在しない"))
            return True

        if not fissures:
            self.result.append(Result(self.respond_to, "現在なし"))
            return True

        for fissure in fissures:
            solnode = database.db_get_solnode(fissure['Node'])
            missiontype = database.db_get_missiontypes(fissure['MissionType'])

            now = pendulum.now()
            expiry = pendulum.from_timestamp(int(fissure['Expiry']['$date']['$numberLong']) / 1000)
            remain = parse_time(expiry - now)

            message = "場所: {value} - 勢力: {enemy} ({missiontype}) - 残り: {remain}".format(
                remain=remain,
                missiontype=missiontype,
                **solnode)

            self.result.append(Result(self.respond_to, message))

        return True

    @activated_at(['!b', '!baro'])
    def baro_ki_teer(self, _) -> bool:
        """
        Baro Ki'Teerが到着したかどうか、到着していたらアイテム一覧を表記
        :param _: 未使用
        :return: 完了したかどうか
        """

        baro = network.get_baro_ki_teer()
        if not baro:
            self.result.append(
                Result(self.respond_to, '失敗: APIとの通信に失敗した 鯖が死んでる説ある')
            )

            return True

        now = pendulum.now()
        activation = pendulum.from_timestamp(int(baro["Activation"]["$date"]["$numberLong"]) / 1000)
        expiry = pendulum.from_timestamp(int(baro["Expiry"]["$date"]["$numberLong"]) / 1000)

        position = database.db_get_solnode(baro['Node'])["value"]

        if activation > now: # Baro Ki Teerは来てない
            date = parse_time(activation - now)
            self.result.append(Result(self.respond_to, 'Baro Ki\'Teer はまだ来てない'))
            self.result.append(
                Result(
                    self.respond_to, '{pos} にくるまで残り: {time}'.format(pos=position,
                                                                   time=date)
                )
            )
            return True

        else:
            if not self.is_privmsg:
                self.result.append(Result(self.message_in, '成功: 長文になるから個人チャット見ろ'))

            time = parse_time(expiry - now)
            self.result.append(Result(self.sender, 'Baro Ki\'Teer は到着している 残り: {time}'.format(time=time)))
            self.result.append(Result(self.sender, '場所: {pos}'.format(pos=position)))

            for item in baro['Manifest']:
                res = Result(
                    self.sender,
                    '{name} - {d} デュカット + {c} クレジット'.format(
                        name=database.db_translate_item(item['ItemType']),
                        d=item["PrimePrice"],
                        c=item["RegularPrice"]
                    )
                )

                self.result.append(res)

        return True

    @activated_at(['!o', '!oper'])
    def oper(self, name) -> bool:
        """
        !o or !oper [ホスト名] で指定されたホストに自動オペレータを追加する
        :param name: ホスト名
        :return: 成功したか否か
        """

        current_opers = database.db_get_set('Oper', 'Oper')

        if not name:
            self.result.append(Result(self.respond_to, '失敗: 引数がないよ　（使い方 - !oper ホスト）'))
            return True

        if name == 'list':
            self.result.append(Result(self.respond_to, '現在のオペレータ一覧 - '))
            for oper in current_opers:
                self.result.append(Result(self.respond_to, '{}'.format(oper.decode())))

            return True

        if not database.db_ismember_set('Oper', 'Oper', self.sender_host):
            self.result.append(Result(self.respond_to, '失敗: お前権限持ってねえじゃねえかよ'))
            return True


        try:
            hashed, cloaked = name.split('_')
            binascii.unhexlify(hashed.encode())
            if not cloaked == 'cloaked':
                raise binascii.Error()

        except (binascii.Error, ValueError):
            self.result.append(Result(self.respond_to, '失敗: 引数がホストとして判定できなかったぞ'))
            self.result.append(Result(self.respond_to, 'ホストは"[8桁の16進数]_cloaked"の形をしてるはずだ'))
            return True

        if database.db_ismember_set('Oper', 'Oper', name):
            if name == database.FORCE_OPER:
                self.result.append(Result(self.respond_to, '失敗: 強制オペレータは削除できない'))
                return True

            deled = database.db_del_set('Oper', 'Oper', name)

            if name.encode() not in deled:
                self.result.append(Result(self.respond_to, '成功: {name}が削除された'.format(name=name)))
                return True

        else:
            if len(current_opers) >= 5:
                self.result.append(Result(self.respond_to, '失敗: 量が多すぎる　何人か消せ'))
                return True

            added = database.db_add_set('Oper', 'Oper', name)

            if name.encode() in added:
                self.result.append(Result(self.respond_to, '成功: {name}が追加された'.format(name=name)))
                return True

        return True

    @activated_at(['!u', '!help', '!usage'])
    def usage(self, _) -> bool:
        if not self.is_privmsg:
            self.result.append(Result(self.message_in, '成功: 長文になるから個人チャット見ろ'))

        msg = [
            'WfBot v0.0.1 Alpha ( Python 3.6 + Redis 2.0.4 + Docker )',
            '問題が発生したらseyarokaに言え',
            '',
            '検索系 - ',
            '!p or !price [アイテム名] : アイテムのプラチナ相場を検索',
            '!d or !drop [アイテム名] : アイテムのドロップ率の検索',
            '!riven [武器名] : アイテムのRivenの相場を検索',
            '!relic [アイテム名] : 指定アイテムを落とすレリック名を検索',
            '',
            'チェック系 - ',
            '!f or !fissure [ランク名 or 数字] : 現在の亀裂チェック',
            '!b or !baro : Baro Ki\'Teerのチェック',
            '',
            'ユーティリティ系 - ',
            '!usage or !u or !help : これ',
            '!o or !oper [list / ホスト名] : 指定したホストが接続した際、オペレータ権限を付与させる',
            '',
            '検索時のノウハウ - ',
            '!p Neur* : 「Neur」から始まる単語をすべて検索',
            '!p Prime -Expel : 「Expel」を含む結果を全て除外',
            '!p Nyx (Neuroptics | Set) : 「Neuroptics」か「Set」を含む結果を検索',
            '!p "Maim" : 「Maim」で完結し完全に一致する結果のみ検索'
        ]

        for msg_ in msg:
            self.result.append(Result(self.sender, msg_))

        return True

import json
import requests
import pendulum
from typing import List
from ircframebot.settings import WORLDSTATE_URL

ERAS = {
    'lith': {'lith', 't1', '1', 'l'},
    'meso': {'meso', 't2', '2', 'm'},
    'neo': {'neo', 't3', '3', 'n'},
    'axi': {'axi', 't4', '4', 'a'},
}

def fissure(era: str) -> List[str]:
    if era is None:
        return ["種類を指定しろ", ]

    try:
        data = requests.get(WORLDSTATE_URL, allow_redirects=False, timeout=5)

    except:
        return ["Warframeサーバーが死んでる", ]

    if data.status_code != 200:
        return ["200 OK以外が帰ってきた", ]

    data = json.loads(data.text)
    data = data["ActiveMissions"]  # Fissure
    era = era.lower()

    results = []
    if era in ERAS['lith']:
        fissures = [i for i in data if i["Modifier"] == "VoidT1"]
        results.append("LITHの亀裂一覧:")

    elif era in ERAS['meso']:
        fissures = [i for i in data if i["Modifier"] == "VoidT2"]
        results.append("MESOの亀裂一覧:")

    elif era in ERAS['neo']:
        fissures = [i for i in data if i["Modifier"] == "VoidT3"]
        results.append("NEOの亀裂一覧:")

    elif era in ERAS['axi']:
        fissures = [i for i in data if i["Modifier"] == "VoidT4"]
        results.append("AXIの亀裂一覧:")

    else:
        return ["種類の指定がどれにも該当しない", ]

    if not fissures:
        results.append('現在なし')
        return results

    for i in data:
        area = self.local_data["solNode"][i["Node"]]["value"]
        faction = self.local_data["solNode"][i["Node"]]["enemy"]
        type = self.local_data["solNode"][i["Node"]]["type"]

        current = datetime.datetime.now()
        expiry = datetime.datetime.fromtimestamp(int(i["Expiry"]["$date"]["$numberLong"]) / 1e3)

        remaintime = calc_time(current, expiry)

        say_word = "場所: {area} - 敵勢力: {faction} - 時間: {remain}".format(area=area,
                                                                       faction=faction,
                                                                       remain=remaintime)
        results.append(say_word)

    return results
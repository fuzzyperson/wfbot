class ModDrop:
    """
    Superclass for mod drops
    """
    def __init__(self, name):
        self.name = name
        self.chance = {
            "dropped_by": [],
            "total_chance": []
            }

    def add_chance(self, dropped_by, total_chance):
        self.chance["dropped_by"].append(dropped_by)
        self.chance["total_chance"].append(total_chance)

    def sorting(self):
        c = zip(self.chance["total_chance"], self.chance["dropped_by"])
        c = sorted(c, key=lambda x: x[0], reverse=True)
        c = c[:3]
        self.chance["total_chance"], self.chance["dropped_by"] = zip(*c)

class Relic:
    def __init__(self, era, name, drops):
        self.era = era
        self.name = name
        self.drops = drops

class RelicDrop:
    def __init__(self, relic, part, chance):
        self.relic = relic
        self.part = part
        self.chance = chance
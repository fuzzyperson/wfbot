
import json
import requests

class Price:
    def __init__(self, item_name, url):
        self.item_name = item_name
        self.url = url

    def __len__(self):
        return len(self.item_name)

class PriceFactory:
    class Meta:
        model = Price

    def __init__(self, market_url, marketapi_url):
        self.market_url = market_url
        self.marketapi_url = marketapi_url

    def fetch_prime_value(self, url_name):

        try:
            data = requests.get(self.marketapi_url + url_name + "/orders",
                                allow_redirects=False, timeout=5)
        except Exception as e:
            return -2

        if data.status_code != 200:
            return -2

        data = json.loads(data.text)
        try:
            data = data["payload"]["orders"]
            if not data:
                return -1

            data = [i["platinum"] for i in data if i["order_type"] == "sell" and i["user"]["status"] == "ingame"]
            if not data:
                return -1

            return min(data)

        except Exception as e:
            print("Error")
            return -2

    def price(self, item_predict=None, target=None):
        item_name = ""
        url_name = ""
        if target is not None:
            self.maybe_target[target] = []
        else:
            self.maybe_list = []

        if self.queue >= 8:
            self.say("一定時間内にコマンド飛ばしすぎ", target)
            return -1

        if self.updating_market:
            self.say("マーケット情報のアップデート中", target)
            return -1

        if item_predict is None:
            self.say("アイテム名を指定しろ", target)
            return -1

        else:
            success, maybe = False, False
            self.queue += 1

            # Turn name into lower case
            target_item_name = item_predict.lower()

            for item_key, item in self.item_dict.items():
                if target_item_name == item_key.lower():
                    success = True

                    item_name = item_key
                    url_name = item['url_name']

                    break

                elif item_key.lower().find(target_item_name) != -1:
                    maybe = True

                    _obj = Price(item_key,
                                 item["url_name"])

                    if target is not None:
                        self.maybe_target[target].append(_obj)

                    else:
                        self.maybe_list.append(_obj)

            if not success:
                if not maybe:
                    self.say("失敗:該当のアイテムが存在しない", target)
                    return -1

                else:
                    _skip = False

                    if target is not None:
                        if len(self.maybe_target[target]) <= 1:
                            print(self.maybe_target[target])
                            item_name = '予測 - ' + self.maybe_target[target][0].item_name
                            url_name = self.maybe_target[target][0].url_name
                            success = True
                            maybe = False
                            _skip = True

                    else:
                        if len(self.maybe_list) <= 1:
                            item_name = '予測 - ' + self.maybe_list[0].item_name
                            url_name = self.maybe_list[0].url_name
                            success = True
                            maybe = False
                            _skip = True

                    if not _skip:
                        self.say("成功:目的のアイテムを!s [数字] で選んでね", target)
                        if target is not None:
                            self.maybe_target[target] = self.maybe_target[target][:3]
                            for i, choice in enumerate(self.maybe_target[target]):
                                self.say(str(i) + ":" + choice.item_name, target)
                        else:
                            self.maybe_list = self.maybe_list[:3]
                            for i, choice in enumerate(self.maybe_list):
                                self.say(str(i) + ":" + choice.item_name)

                        return 0

            if success:
                result = self.fetch_prime_value(url_name)

                if result >= 0:
                    searched_text = "{item_name}: {price} pt".format(item_name=item_name, price=result)
                    self.say("成功: " + searched_text, target)

                elif result == -1:
                    self.say("失敗:アイテムの売り注文がない", target)

                elif result == -2:
                    self.say("失敗:エラー発生", target)

                if target is not None:
                    self.maybe_target[target] = []
                else:
                    self.maybe_list = []

                return 1

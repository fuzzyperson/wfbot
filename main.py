# coding: utf-8

from ircframebot.bot import MarketBot
from ircframebot import database
from apscheduler.schedulers.background import BackgroundScheduler

#Todo !a !arb: 仲裁情報を吐く

#Todo !oper : BOTが生きている時、オペレータ権限を付与させる
#Todo !notify [arb or fissure-era] [only=フィルタ not=フィルタ] 特定のフィルタに一致した情報が出た時通知する

#FixMe !dでMODしか検索対象として出てこない問題


def main():
    database.db_data_update()
    if not database.db_ismember_set('Oper', 'Oper', database.FORCE_OPER):
        database.db_add_set('Oper', 'Oper', database.FORCE_OPER)
    scheduler = BackgroundScheduler()
    bot = MarketBot()

    scheduler.add_job(bot.update, 'cron', hour='3', minute='0', second='0')
    scheduler.start()
    bot.start()


if __name__ == "__main__":
    main()
